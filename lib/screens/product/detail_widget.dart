import 'package:flutter/material.dart';
import 'package:kilomania_virtual/models/product.dart';
import 'package:provider/provider.dart';

import 'package:kilomania_virtual/models/product_detail.dart';

class DetailWidget extends StatelessWidget {
  final ProductDetail detail;

  const DetailWidget({this.detail});

  @override
  Widget build(BuildContext context) {
    final product = context.watch<Product>();
    final selected = detail == product.selectedDetail;

    Color color;
    if (!detail.hasStock) {
      color = Colors.red.withAlpha(50);
    } else if (selected) {
      color = Theme.of(context).primaryColor;
    } else {
      color = Colors.grey;
    }

    return GestureDetector(
      onTap: () {
        if (detail.hasStock) {
          product.selectedDetail = detail;
        }
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: !detail.hasStock ? Colors.red.withAlpha(50) : Colors.grey,
          ),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: color,
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              child: Text(
                detail.name.toUpperCase(),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                'R\$ ${detail.price.toStringAsFixed(2)}',
                style: TextStyle(
                  color: color,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
