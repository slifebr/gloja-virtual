import 'package:flutter/material.dart';
import 'package:kilomania_virtual/models/product_manager.dart';
import 'package:kilomania_virtual/screens/login/login_screen.dart';
import 'package:kilomania_virtual/screens/product/product_screen.dart';
import 'package:kilomania_virtual/screens/signup/signup_screen.dart';
import 'package:provider/provider.dart';

import 'package:kilomania_virtual/models/user_manager.dart';
import 'package:kilomania_virtual/screens/base/base_screen.dart';

import 'models/product.dart';

Future<void> main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Color colorApp = const Color.fromARGB(255, 4, 125, 141);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => UserManager(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => ProductManager(),
          lazy: false,
        ),
      ],
      child: MaterialApp(
        title: 'Kilomania Tecidos',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: colorApp,
          scaffoldBackgroundColor: colorApp,
          visualDensity: VisualDensity.adaptivePlatformDensity,

          //retirar a elevação entre a barra e o conteudo
          appBarTheme: const AppBarTheme(
            elevation: 0,
          ),
        ),
        initialRoute: '/base',
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case '/login':
              return MaterialPageRoute(builder: (_) => LoginScreen());
            case '/signup':
              return MaterialPageRoute(builder: (_) => SignupScreen());
            case '/product':
              return MaterialPageRoute(builder: (_) => ProductScreen(
                settings.arguments as Product
              ));
            case '/base':
            default:
              return MaterialPageRoute(builder: (_) => BaseScreen());
          }
        },
      ),
    );
  }
}
