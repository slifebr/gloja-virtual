class ProductDetail {
  String name;
  num price;
  num stock;

   ProductDetail.fromMap(Map<String, dynamic> map){
     name = map['name'] as String;
     price = map['price'] as num;
     stock = map['stock'] as num;
  }

  // checar se o produto tem estoque
  bool get hasStock => stock > 0;



  @override
  String toString() {
    return 'ProductDetail{name: $name, price: $price, stock: $stock}';
  }
}