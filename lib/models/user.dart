import 'package:cloud_firestore/cloud_firestore.dart';

class User {

  String id;
  String name;
  String email;
  String password;

  String confirmPassword;

  // funcao que traz o documento como referencia
  DocumentReference get firestoreRef => Firestore.instance.document('users/$id');

  //constructor
  User({this.id, this.name, this.email, this.password, this.confirmPassword});

  //constructor com Document
  User.fromDocument(DocumentSnapshot document){
    id = document.documentID;
    name = document.data['name'] as String;
    email = document.data['email'] as String;
  }

   //salvar colecao no firebase
  Future<void> saveData() async {
    await firestoreRef.setData(toMap());
  }
    //exemplo sem DocumentReference
    /*
   Future<void> saveData() async {
     await Firestore.instance
         .collection('users')
         .document(id)
         .setData(toMap());
   }
   */

   // funcao para mapear dados do user
   Map<String, dynamic> toMap() {
     return {
       'name': name,
       'email': email,
     };
   }
  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, password: $password, confirmPassword: $confirmPassword}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}