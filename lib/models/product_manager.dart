import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:kilomania_virtual/models/product.dart';

class ProductManager extends ChangeNotifier {
  //constructor
  ProductManager(){
    _loadAllProducts();

  }

  // instacia do firestore
  final Firestore firestore = Firestore.instance;

  List<Product> allProducts = [];

  String _search = '';
  String get search => _search;
  set search(String value){
    _search = value;
    notifyListeners();
  }

  // fazer filtro conforme critério
  List<Product> get filteredProducts{
    final List<Product> filtered = [];
    if(search.isEmpty){
      filtered.addAll(allProducts);
    }else {
      filtered.addAll(allProducts.where((p) => p.name.toLowerCase().contains(search)));
    }
    return filtered;
  }

  Future<void> _loadAllProducts() async {
    final QuerySnapshot snapshot = await firestore.collection('products').getDocuments();
    allProducts = snapshot.documents.map((e) => Product.fromDocuments(e)).toList();
    notifyListeners();
  }


}