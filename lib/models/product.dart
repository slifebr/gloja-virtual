import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:kilomania_virtual/models/product_detail.dart';

class Product extends ChangeNotifier{
  String id;
  String internalCode;
  String name;
  String description;
  String observation;
  List<String> images;
  List<ProductDetail> details;
  ProductDetail _selectedDetail;

  Product.fromDocuments(DocumentSnapshot doc) {
    id = doc.documentID;
    internalCode = doc['internal_code'] as String;
    name = doc['name'] as String;
    description = doc['description'] as String;
    observation = doc['observation'] as String;
    images = List<String>.from(doc.data['images'] as List<dynamic>);

    //TODO: MUDAR NOME DO CAMPO NO FIREBASE PARA "UNIDADE" => "DETAIL"
    details = (doc.data['unidade'] as List<dynamic> ?? [])
        .map((detail) => ProductDetail.fromMap(detail as Map<String, dynamic>))
        .toList();
  } // fromDocuments

  ProductDetail get selectedDetail => _selectedDetail;

  set selectedDetail(ProductDetail detail) {
    _selectedDetail = detail;
    notifyListeners();
  }

  num get totalStock{
    num stock = 0.0;
    for (final detail in details){
      stock += detail.stock;
    }
    return stock;
  }

  bool get hasStock{
    return totalStock > 0.0;
  }

}
